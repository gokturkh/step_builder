<?php

declare(strict_types=1);

namespace HG\Classes;

use HG\Interfaces\StepA;
use HG\Interfaces\StepB;
use HG\Interfaces\StepCD;

/**
 * Class StepBuilder
 *
 * @package HG\Classes
 */
final class StepBuilder implements StepA, StepB, StepCD
{
    /**
     * StepBuilder constructor.
     */
    private function __construct()
    {
    }

    /**
     * Do some initial stuff.
     * Only init() is callable first.
     * After init(), only methodA() can be called.
     *
     * @return \HG\Interfaces\StepA
     */
    public static function init(): StepA
    {
        return new static();
    }

    /**
     * After methodA(), only methodB() can be called.
     *
     * @return \HG\Interfaces\StepB
     */
    public function methodA(): StepB
    {
        echo "MethodA" . PHP_EOL;
        return $this;
    }

    /**
     * After methodB(), methodC() or methodD() can be called.
     *
     * @return \HG\Interfaces\StepCD
     */
    public function methodB(): StepCD
    {
        echo "MethodB" . PHP_EOL;
        return $this;
    }

    /**
     * No method can be called after methodC().
     */
    public function methodC(): void
    {
        echo "MethodC" . PHP_EOL;
    }

    /**
     * No method can be called after methodD().
     */
    public function methodD(): void
    {
        echo "MethodD" . PHP_EOL;
    }
}

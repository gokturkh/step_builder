<?php

declare(strict_types=1);

namespace HG\Interfaces;

/**
 * Interface StepB
 *
 * @package HG\Interfaces
 */
interface StepB
{
    /**
     * @return \HG\Interfaces\StepCD
     */
    public function methodB(): StepCD;
}

<?php

declare(strict_types=1);

namespace HG\Interfaces;

/**
 * Interface StepA
 *
 * @package HG\Interfaces
 */
interface StepA
{
    /**
     * @return \HG\Interfaces\StepB
     */
    public function methodA(): StepB;
}

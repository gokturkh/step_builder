<?php

declare(strict_types=1);

namespace HG\Interfaces;

/**
 * Interface StepCD
 *
 * @package HG\Interfaces
 */
interface StepCD
{
    /**
     *
     */
    public function methodC(): void;

    /**
     *
     */
    public function methodD(): void;
}

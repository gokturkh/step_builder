<?php

declare(strict_types=1);

use HG\Classes\StepBuilder;

require __DIR__ . '/vendor/autoload.php';

$step_builder = StepBuilder::init();
$step_builder->methodA()
             ->methodB()
             ->methodD();

# Step Builder pattern demonstration.
> A simple step builder demonstration..

## Installation
```sh
composer install
```

## Usage example
```php
$step_builder = StepBuilder::init();
$step_builder->methodA()
             ->methodB()
             ->methodD();
```

## Meta

Hayrettin Gokturk – hayrettin.gokturk@gmail.com